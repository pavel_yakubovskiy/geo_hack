#!/usr/bin/env bash

cd data/

# Downloading data
mkdir -p raw_data/
kaggle competitions download -c airbus-ship-detection -p raw_data/

## Extracting data
mkdir -p dataset/test/
unzip -q raw_data/test_v2.zip -d dataset/test/

mkdir -p dataset/train_overlapped/
unzip -q raw_data/train_v2.zip -d dataset/train_overlapped/

unzip -q raw_data/train_ship_segmentations_v2.csv.zip -d dataset/

# Extracting submission format
mkdir -p submissions/
cp raw_data/sample_submission_v2.csv -d submissions/

