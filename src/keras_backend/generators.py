import torch
from functools import wraps
from torch.utils.data import DataLoader


@wraps(DataLoader)
def segmentation_generator(*args, **kwargs):
    data_loader = DataLoader(*args, **kwargs)
    while True:
        for batch in data_loader:
            images = batch['image']
            masks = batch['mask']
            yield images.numpy(), masks.numpy()


@wraps(DataLoader)
def classification_generator(*args, **kwargs):
    data_loader = DataLoader(*args, **kwargs)
    while True:
        for batch in data_loader:
            images = batch['image']
            labels = batch['label']
            yield images.numpy(), labels.numpy()

@wraps(DataLoader)
def cls_seg_generator(*args, **kwargs):
    data_loader = DataLoader(*args, **kwargs)
    while True:
        for batch in data_loader:
            images = batch['image'].numpy()
            masks = batch['mask'].numpy()
            labels = batch['label'].numpy()
            yield images, {'cls': labels, 'seg': masks}


def to_keras_generator(data_loader, keys):
    while True:
        for batch in data_loader:
            formed_batch = []
            for key in keys:
                v = batch[key]
                if isinstance(v, torch.ByteTensor):
                    v = v.numpy()
                formed_batch.append(v)
            yield formed_batch