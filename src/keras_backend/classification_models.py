from keras.layers import Dense, Dropout, GlobalMaxPooling2D, GlobalAveragePooling2D, Lambda, Add, Activation
from keras.models import Model
from segmentation_models.backbones import get_backbone


def get_classifier(**params):
    base_model = get_backbone(params['architecture'], input_shape=(None, None, 3), **params['base_model'])
    x = base_model.output


    pooling = params.get('pooling', 'max')
    if pooling == 'max':
        x = GlobalMaxPooling2D()(x)
    elif pooling == 'avg':
        x = GlobalAveragePooling2D()(x)
    elif pooling == 'max+avg':
        x1 = GlobalMaxPooling2D()(x)
        x2 = GlobalAveragePooling2D()(x)
        x = Add()([x1, x2])
        x = Lambda(lambda x: x / 2)(x)
    else:
        raise ValueError('Wrong pooling - {}, use `max`, `avg`, `max+avg`'.format(pooling))

    x = Dense(512)(x)
    
    dropout = params.get('dropout', 0)
    if dropout:
        x = Dropout(dropout)(x)
        
    x = Dense(params['classes'])(x)
    x = Activation(params['activation'])(x)
    
    model = Model(base_model.input, x)
    model.name = base_model.name

    freeze = params.get('freeze', False)
    if freeze:
        for l in base_model.layers:
            l.trainable = False
        print('Base model freezed!')
    
    return model
