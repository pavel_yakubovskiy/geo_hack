from functools import wraps
import sys

import keras
import keras.backend as K
from keras.losses import binary_crossentropy
from keras.losses import categorical_crossentropy
from keras.utils.generic_utils import get_custom_objects
import tensorflow as tf

from .metrics import iou_score, custom_iou_score
from .metrics import f1_score, custom_f_score

custom_losses = sys.modules[__name__].__dict__


def get_losses_dict(names_dict):
    losses = {k: get_loss(v) for k, v in names_dict.items()}
    return losses

def get_losses(names):
    losses = [get_loss(name) for name in names]
    return losses


def get_loss(name):
    
    if name in custom_losses.keys():
        l = custom_losses[name]
    else:
        l = getattr(keras.losses, name)
       
    return l

# ============================== Jaccard Losses ==============================

def jaccard_loss(gt, pr):
    return 1 - iou_score(gt, pr)


def _bce_jaccard_loss(gt, pr, bce_weight=1, smooth=1):

    iou_score = custom_iou_score(smooth=smooth)

    bce = K.mean(binary_crossentropy(gt, pr))
    jaccard_loss = 1 - iou_score(gt, pr)

    return bce_weight * bce + jaccard_loss


def _cce_jaccard_loss(gt, pr, bce_weight=1, class_weights=1, smooth=1):

    iou_score = custom_iou_score(class_weights=class_weights, smooth=smooth)

    cce = categorical_crossentropy(gt, pr) * class_weights
    cce = K.mean(cce)
    jaccard_loss = 1 - iou_score(gt, pr)
    return bce_weight * cce + jaccard_loss


def bce_jaccard_loss(gt, pr):
    return _bce_jaccard_loss(gt, pr, bce_weight=1, smooth=1)


def cce_jaccard_loss(gt, pr):
    return _cce_jaccard_loss(gt, pr, bce_weight=1, class_weights=1, smooth=1)


### Custom jaccard losses

def custom_jaccard_loss(class_weights=1, smooth=1):
    metric = custom_iou_score(class_weights=class_weights, smooth=smooth)
    def loss(gt, pr):
        return 1 - metric(gt, pr)
    return loss

def custom_bce_jaccard_loss(bce_weight=1, smooth=1):
    def loss(gt, pr):
        return _bce_jaccard_loss(gt, pr, bce_weight=bce_weight, smooth=smooth)
    return loss


def custom_cce_jaccard_loss(bce_weight=1, class_weights=1, smooth=1):
    def loss(gt, pr):
        return _cce_jaccard_loss(gt, pr, bce_weight=bce_weight, class_weights=class_weights, smooth=smooth)
    return loss


# ============================================ Focal losses ============================================


def focal_loss(y_true, y_pred, gamma=1, alpha=1):
    
    y_true = K.flatten(y_true)
    y_pred = K.flatten(y_pred)
    
    eps = 1e-6
    
    pt_1 = tf.where(tf.equal(y_true, 1), y_pred, tf.ones_like(y_pred))
    pt_0 = tf.where(tf.equal(y_true, 0), y_pred, tf.zeros_like(y_pred))
    
    pt_0 = K.clip(pt_0, eps, 1.- eps)
    pt_1 = K.clip(pt_1, eps, 1.- eps)
    
    foreground = alpha * K.pow(1. - pt_1, gamma) * K.log(pt_1)
    background = (1 - alpha) * K.pow(pt_0, gamma) * K.log(1. - pt_0)
    loss =  - K.mean(foreground + background)
    
    return loss

def focal_loss_1(y_true, y_pred):
    return focal_loss(y_true, y_pred, gamma=2, alpha=0.75)

def focal_loss_2(y_true, y_pred):
    return focal_loss(y_true, y_pred, gamma=2, alpha=0.25)

def focal_1_jaccard_loss(y_true, y_pred):
    return focal_loss_1(y_true, y_pred) + jaccard_loss(y_true, y_pred)

def focal_2_jaccard_loss(y_true, y_pred):
    return focal_loss_2(y_true, y_pred) + jaccard_loss(y_true, y_pred)



# Update custom objects
get_custom_objects().update({
    'jaccard_loss': jaccard_loss,
    'bce_jaccard_loss': bce_jaccard_loss,
    'cce_jaccard_loss': cce_jaccard_loss,
})