from functools import wraps
from segmentation_models import Unet

from keras.layers import Multiply
from keras.models import Model

from keras.layers import Dense, Dropout, GlobalMaxPooling2D, GlobalAveragePooling2D, Lambda, Add, Activation


def encoder_last_layer(model, decoder_first_layer='decoder_stage0_upsample'):
    for i, l in enumerate(model.layers):
        if l.name == decoder_first_layer:
            return model.layers[i-1]
    raise ValueError('First layer of decoder is not found: {}'.format(decoder_first_layer))

    
    
def get_classifier_head(input, **params):
    
    x = input
    
    pooling = params.get('pooling', 'max')
    if pooling == 'max':
        x = GlobalMaxPooling2D()(x)
    elif pooling == 'avg':
        x = GlobalAveragePooling2D()(x)
    elif pooling == 'max+avg':
        x1 = GlobalMaxPooling2D()(x)
        x2 = GlobalAveragePooling2D()(x)
        x = Add()([x1, x2])
        x = Lambda(lambda x: x / 2)(x)
    else:
        raise ValueError('Wrong pooling - {}, use `max`, `avg`, `max+avg`'.format(pooling))

    x = Dense(512)(x)
    
    dropout = params.get('dropout', 0)
    if dropout:
        x = Dropout(dropout)(x)
        
    x = Dense(params['classes'])(x)
    x = Activation(params['activation'], name='cls')(x)
    
    return x
    
    
def DSUnet(segmentation_params, classification_params, multiply=True):

    unet = Unet(**segmentation_params)
    
    encoder_output = encoder_last_layer(unet).output
    cls = get_classifier_head(encoder_output, **classification_params)
    
    if multiply:
        seg = Multiply(name='seg')([unet.output, cls])
    else:
        unet.layers[-1].name = 'seg'
        seg = unet.output
    
    model = Model(unet.input, outputs=[cls, seg])
    model.name = 'ds-{}'.format(unet.name)
    return model
                                            
    
    