import sys, os
import numpy as np

import keras
from keras.models import Model
from keras import backend as K
from keras.callbacks import Callback

custom_callbacks = sys.modules[__name__].__dict__

LOGS_DIR = None
CHECKPOINTS_DIR = None


def get_callbacks(config, logs_dir=None, checkpoints_dir=None):
    callbacks = []

    global LOGS_DIR
    global CHECKPOINTS_DIR
    LOGS_DIR = logs_dir
    CHECKPOINTS_DIR = checkpoints_dir

    for callback, params in config.items():

        if callback in custom_callbacks.keys():

            c = custom_callbacks[callback](**params)
            callbacks.append(c)

        else:
            c = getattr(keras.callbacks, callback)(**params)
            callbacks.append(c)

    return callbacks


# Your custom callbacks goes here:
#
#
class ModelRelease(keras.callbacks.Callback):
    def __init__(self, release_epoch):
        self.release_epoch = release_epoch
        super().__init__()

    def on_epoch_end(self, epoch, logs=None):

        if epoch == self.release_epoch:

            # set trainable for all layers
            for layer in self.model.layers:
                layer.trainable = True

            # recompile model
            self.model.compile(self.model.optimizer,
                               self.model.loss,
                               self.model.metrics)

            print('Epoch: {}. All layers released!'.format(epoch))


class ModelCheckpoint(keras.callbacks.ModelCheckpoint):
    def __init__(self, cycle_epochs=None, **params):

        realtive_path = params.pop('filepath')
        filepath = os.path.join(CHECKPOINTS_DIR, 'models', realtive_path)

        base_dir = os.path.dirname(filepath)
        os.makedirs(base_dir, exist_ok=True)

        self.mode = params.get('mode', 'auto')
        self.cycle_epochs = cycle_epochs if cycle_epochs is not None else []

        super().__init__(filepath, **params)

    def on_epoch_begin(self, epoch, logs={}):

        # drop best results for each cycle (for multi cycle lerning and snapshot ensambling)
        if epoch in self.cycle_epochs:
            print('New cylce, best checkpoint results dropped.')
            if self.mode == 'min':
                self.monitor_op = np.less
                self.best = np.Inf
            elif self.mode == 'max':
                self.monitor_op = np.greater
                self.best = -np.Inf
            else:
                if 'acc' in self.monitor or self.monitor.startswith('fmeasure'):
                    self.monitor_op = np.greater
                    self.best = -np.Inf
                else:
                    self.monitor_op = np.less
                    self.best = np.Inf


class MultiGPUModelCheckpoint(ModelCheckpoint):
    def set_model(self, model):

        found = False
        for layer in model.layers:
            if isinstance(layer, Model):
                self.model = layer
                found = True
                print('Multi-gpu mode, sub model for saving have been found!')
                break
        if not found:
            self.model = model


class TensorBoard(keras.callbacks.TensorBoard):
    def __init__(self, **params):
        os.makedirs(LOGS_DIR, exist_ok=True)
        super().__init__(LOGS_DIR, **params)


class CyclicLR(Callback):
    """This callback implements a cyclical learning rate policy (CLR).
    The method cycles the learning rate between two boundaries with
    some constant frequency, as detailed in this paper (https://arxiv.org/abs/1506.01186).
    The amplitude of the cycle can be scaled on a per-iteration or
    per-cycle basis.
    This class has three built-in policies, as put forth in the paper.
    "triangular":
        A basic triangular cycle w/ no amplitude scaling.
    "triangular2":
        A basic triangular cycle that scales initial amplitude by half each cycle.
    "exp_range":
        A cycle that scales initial amplitude by gamma ** (cycle iterations) at each
        cycle iteration.
    For more detail, please see paper.

    # Example
        ```python
            clr = CyclicLR(base_lr=0.001, max_lr=0.006,
                                step_size=2000., mode='triangular')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```

    Class also supports custom scaling functions:
        ```python
            clr_fn = lambda x: 0.5*(1+np.sin(x*np.pi/2.))
            clr = CyclicLR(base_lr=0.001, max_lr=0.006,
                                step_size=2000., scale_fn=clr_fn,
                                scale_mode='cycle')
            model.fit(X_train, Y_train, callbacks=[clr])
        ```
    # Arguments
        base_lr: initial learning rate which is the
            lower boundary in the cycle.
        max_lr: upper boundary in the cycle. Functionally,
            it defines the cycle amplitude (max_lr - base_lr).
            The lr at any cycle is the sum of base_lr
            and some scaling of the amplitude; therefore
            max_lr may not actually be reached depending on
            scaling function.
        step_size: number of training iterations per
            half cycle. Authors suggest setting step_size
            2-8 x training iterations in epoch.
        mode: one of {triangular, triangular2, exp_range}.
            Default 'triangular'.
            Values correspond to policies detailed above.
            If scale_fn is not None, this argument is ignored.
        gamma: constant in 'exp_range' scaling function:
            gamma**(cycle iterations)
        scale_fn: Custom scaling policy defined by a single
            argument lambda function, where
            0 <= scale_fn(x) <= 1 for all x >= 0.
            mode parameter is ignored
        scale_mode: {'cycle', 'iterations'}.
            Defines whether scale_fn is evaluated on
            cycle number or cycle iterations (training
            iterations since start of cycle). Default is 'cycle'.
    """

    def __init__(self, base_lr=0.001, max_lr=0.006, step_size=2000., mode='triangular',
                 gamma=1., scale_fn=None, scale_mode='cycle', start_epoch=0):
        super(CyclicLR, self).__init__()

        self.base_lr = base_lr
        self.max_lr = max_lr
        self.step_size = step_size
        self.mode = mode
        self.gamma = gamma
        self.start_epoch = start_epoch
        if scale_fn is None:
            if self.mode == 'triangular':
                self.scale_fn = lambda x: 1.
                self.scale_mode = 'cycle'
            elif self.mode == 'triangular2':
                self.scale_fn = lambda x: 1 / (2. ** (x - 1))
                self.scale_mode = 'cycle'
            elif self.mode == 'exp_range':
                self.scale_fn = lambda x: gamma ** x
                self.scale_mode = 'iterations'
        else:
            self.scale_fn = scale_fn
            self.scale_mode = scale_mode
        self.clr_iterations = 0.
        self.trn_iterations = 0.
        self.history = {}

        self._reset()

    def _reset(self, new_base_lr=None, new_max_lr=None,
               new_step_size=None):
        """Resets cycle iterations.
        Optional boundary/step size adjustment.
        """
        if new_base_lr is not None:
            self.base_lr = new_base_lr
        if new_max_lr is not None:
            self.max_lr = new_max_lr
        if new_step_size is not None:
            self.step_size = new_step_size
        self.clr_iterations = 0.

    def clr(self):
        cycle = np.floor(1 + self.clr_iterations / (2 * self.step_size))
        x = np.abs(self.clr_iterations / self.step_size - 2 * cycle + 1)
        if self.scale_mode == 'cycle':
            return self.base_lr + (self.max_lr - self.base_lr) * np.maximum(0, (1 - x)) * self.scale_fn(cycle)
        else:
            return self.base_lr + (self.max_lr - self.base_lr) * np.maximum(0, (1 - x)) * self.scale_fn(
                self.clr_iterations)

    def on_epoch_begin(self, epoch, logs=None):
        if epoch == self.start_epoch:
            print('Cyclic LR is activated ...')
            if self.clr_iterations == 0:
                K.set_value(self.model.optimizer.lr, self.base_lr)
            else:
                K.set_value(self.model.optimizer.lr, self.clr())

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs['lr'] = K.get_value(self.model.optimizer.lr)
        self.history.setdefault('lr', []).append(K.eval(self.model.optimizer.lr))

    def on_batch_end(self, epoch, logs=None):

        if epoch >= self.start_epoch:
            logs = logs or {}
            self.trn_iterations += 1
            self.clr_iterations += 1
            K.set_value(self.model.optimizer.lr, self.clr())

            self.history.setdefault('lr', []).append(K.get_value(self.model.optimizer.lr))
            self.history.setdefault('iterations', []).append(self.trn_iterations)

            for k, v in logs.items():
                self.history.setdefault(k, []).append(v)


class ScheduledLR(Callback):
    def __init__(self, factor=0.3, milestones=[], verbose=1):
        self.factor = factor
        self.milestones = milestones
        self.verbose = verbose

    def on_epoch_begin(self, epoch, logs=None):

        if epoch in self.milestones:
            lr = K.get_value(self.model.optimizer.lr) * self.factor
            K.set_value(self.model.optimizer.lr, lr)

        if self.verbose:
            print('Epoch {}. Learining rate {:.6}.'.format(epoch, K.get_value(self.model.optimizer.lr)))

