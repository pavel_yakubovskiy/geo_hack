import albumentations

def albumentations_from_config(config):
    """Parse augmentations"""
    augmentations = []

    for aug, params in config.items():

        if aug.count('OneOf'):
            p = params.pop('p', 0)
            a = albumentations_from_config(params)
            a = albumentations.OneOf(a, p)
            augmentations.append(a)

        else:
            a = getattr(albumentations, aug)
            augmentations.append(a(**params))

    return augmentations


def get_augmenter(config):
    augs = albumentations_from_config(config)
    return albumentations.Compose(augs, p=1.)