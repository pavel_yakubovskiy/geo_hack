import numpy as np
from torch.utils.data import WeightedRandomSampler


def get_sampler(name):

    samplers = {
        'weighted': weighted_sampler,
    }

    return samplers[name]


# def weighted_sampler(df, classes_ratios):
#     labels = np.array(~df['EncodedPixels'].isnull().values, dtype=np.uint8)
#     return _weighted_sampler(labels, classes_ratios)

def weighted_sampler(labels, classes_ratios, n_samples=None):

    frequency = {cls: np.sum(labels == int(cls)) for cls in classes_ratios.keys()}
    sampling_ratios = {cls: classes_ratios[cls] / frequency[cls] for cls in classes_ratios.keys()}
    sampling_probs = [sampling_ratios[str(label)] for label in labels]
    
    if n_samples is None:
        n_samples = len(labels)
        
    sampler = WeightedRandomSampler(sampling_probs, n_samples)
    return sampler
