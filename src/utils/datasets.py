import os
import numpy as np
from torch.utils.data import Dataset

from .io import read_image
from .io import multi_rle_decode

import sys


class ShipDataset(Dataset):

    def __init__(self, ids, root_dir, masks_df=None, mode='train', transform=None):
        self.ids = ids
        self.masks = to_dict(masks_df) if masks_df is not None else None
        self.root_dir = root_dir
        self.mode = mode
        self.transform = transform

    def __len__(self):
        return len(self.ids)


class ShipSegmentationDataset(ShipDataset):

    def __getitem__(self, item):

        image_id = self.ids[item]
        image_path = os.path.join(self.root_dir, image_id)
        image = read_image(image_path)

        sample = {}
        sample['image_id'] = image_id
        sample['image'] = image

        if self.mode == 'train':
            rle_masks = self.masks[image_id]
            mask = multi_rle_decode(rle_masks, image.shape[:2])
            sample['mask'] = mask

        if self.transform is not None:
            sample = self.transform(sample)
            
        return sample


class ShipClassificationDataset(ShipSegmentationDataset):

    def __getitem__(self, item):
        sample = super().__getitem__(item)

        if 'mask' in sample.keys():
            sample['label'] = 1 if sample['mask'].sum() > 0 else 0

        return sample

    
class ShipClassificationDatasetV2(ShipSegmentationDataset):

    def __getitem__(self, item):
        sample = super().__getitem__(item)

        if 'mask' in sample.keys():
            # label is 1 if there is a pixels of traget class
            # excluded pixels near borders (hard to distinguish for network)
            sample['label'] = 1 if zero_border(sample['mask'], 2).sum() > 0 else 0

        return sample

    
ShipClsSegDataset = ShipClassificationDataset


# =============================== Helpers ====================================

def zero_border(arr, w=2):
    arr = arr.copy()
    arr[:w] = 0
    arr[:, :w] = 0
    arr[-w:] = 0
    arr[:, -w:] = 0
    return arr

def to_dict(masks_df):

    if isinstance(masks_df, dict):
        return masks_df

    print('Indexing... ETA - {} sec.'.format(masks_df.shape[0] / 10000))
    masks_dict = (masks_df.fillna('')
                  .groupby('ImageId')['EncodedPixels']
                  .apply(list)
                  .to_dict())
    return masks_dict


def shape_guard(sample):
    """control all images have 3 dims after augmentaions and preprocessing"""
    for k, v in sample.items():
        if isinstance(v, np.ndarray):
            if v.ndim == 2:
                sample[k] = np.expand_dims(v, -1)
    return sample


def make_transform(preprocessing_fn=None, augmenter=None, transforms=None):
    def transform(sample):
        
        if transforms is not None:
            for t in transforms:
                sample = t(sample)
            
        if augmenter is not None:
            sample = augmenter(**sample)

        if preprocessing_fn is not None:
            sample['image'] = preprocessing_fn(sample['image'].astype('float32')).copy()

        sample = shape_guard(sample)
        return sample
    return transform