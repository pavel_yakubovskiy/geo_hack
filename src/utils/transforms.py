import sys
import numpy as np


custom_transforms = sys.modules[__name__].__dict__


def get_transforms(**config):
    transforms = [_get_transform(name)(**params) for name, params in config.items()]
    return transforms

def _get_transform(name):
    return custom_transforms[name]
    
        
# =================================== Crop ===================================

def crop(image, h_off, w_off, h, w):
    assert (h_off + h) <= image.shape[0]
    assert (w_off + w) <= image.shape[1]
    return image[h_off:h_off + h, w_off:w_off + w]


def random_crop_params(image, h, w):
    image_h, image_w = image.shape[:2]
    h_off = np.random.randint(image_h - h)
    w_off = np.random.randint(image_w - w)
    return {'h_off': h_off, 'w_off': w_off, 'h': h, 'w': w}


def class_crop_params(image, h, w):
    image_h, image_w = image.shape[:2]
    n_rows = (image_h - 1) // h + 1
    n_cols = (image_w - 1) // w + 1
    
    # grid of crops
    h_offsets = [h * r for r in range(n_rows)]
    w_offsets = [w * c for c in range(n_cols)]
    
    # correct grid to be in image shape 
    h_offsets = [off if off + h <= image_h else image_h - h for off in h_offsets] 
    w_offsets = [off if off + w <= image_w else image_w - w for off in w_offsets] 
    
    # get non-empty crops by offsets
    offsets = []
    for h_off in h_offsets:
        for w_off in w_offsets:
            sample = crop(image, h_off, w_off, h, w)
            if sample.sum() > 0:
                offsets.append({'h_off': h_off, 'w_off': w_off, 'h': h, 'w': w})
    
    return offsets


# =================================== Shift ===================================

def shift(image, h_off, w_off):
    image = np.roll(image, h_off, axis=0)
    image = np.roll(image, w_off, axis=1)
    return image


def random_shift_params(image):
    h_off = np.random.randint(image.shape[0])
    w_off = np.random.randint(image.shape[1])
    return {'h_off': h_off, 'w_off': w_off}
    
    
# =================================== Transform ===================================  

def CropClassIfExists(h, w, random_shift=True):
    def transform(sample):
        
        image = sample['image']
        mask = sample['mask']
        
        # make random shift of an image
        if random_shift:
            shift_params = random_shift_params(image)
            image = shift(image, **shift_params)
            mask = shift(mask, **shift_params)
        
        # crop image with non empty mask if objects exist
        if mask.sum() == 0:
            crop_params = random_crop_params(mask, h, w) 
        else:
            crop_params = class_crop_params(mask, h, w) # return list of possible non-empty crops
            crop_params = np.random.choice(crop_params) 
            
        image = crop(image, **crop_params)
        mask = crop(mask, **crop_params)
        
        sample['image'] = image
        sample['mask'] = mask
        
        return sample
    return transform