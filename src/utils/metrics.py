import numpy as np
from skimage.morphology import label

# ====================================== Kaggle F2 instance metric ====================================== 

def f2_score(gt, pr):
    """
    Args:
        gt: np.array HxW; uint8, 0 - backgound, 1 - target class
        pr: np.array HxW; uint8, 0 - backgound, 1 - target class
    """
    
    if gt.sum() == 0 and pr.sum() == 0:
        return 1
    elif gt.sum() == 0 and pr.sum() > 0:
        return 0
    elif gt.sum() > 0 and pr.sum() == 0:
        return 0
    
    gt = label(gt, connectivity=1)
    pr = label(pr, connectivity=1)
    #print('GT ships: {}'.format(len(np.unique(gt) - 1)))
    #print('PR ships: {}'.format(len(np.unique(pr) - 1)))
    return calculate_ap(gt, pr, beta=2)


def calculate_ap(gt, pr, beta=1):
    
    labels = gt
    y_pred = pr
        
    # y_pred should also contain background labels
    # y_pred should contain it if it is taken from wt transform
        
    true_objects = len(np.unique(labels))
    pred_objects = len(np.unique(y_pred)) 
    
    # Compute intersection between all objects
    intersection = np.histogram2d(labels.flatten(), y_pred.flatten(), bins=(true_objects, pred_objects))[0]

    # Compute areas (needed for finding the union between all objects)
    area_true = np.histogram(labels, bins = true_objects)[0]
    area_pred = np.histogram(y_pred, bins = pred_objects)[0]
    area_true = np.expand_dims(area_true, -1)
    area_pred = np.expand_dims(area_pred, 0)

    # Compute union
    union = area_true + area_pred - intersection 
    
    # Exclude background from the analysis
    intersection = intersection[1:,1:]
    union = union[1:,1:]
    union[union == 0] = 1e-9

    # Compute the intersection over union
    iou = intersection / union   

    # Loop over IoU thresholds
    prec = []
    #print("Thresh\tTP\tFP\tFN\tPrec.")
    for t in np.arange(0.5, 1.0, 0.05):
        tp, fp, fn = precision_at(t, iou)
        p = (1 + beta**2) * tp / ((1 + beta**2) * tp + fp + beta**2 * fn)
        #print("{:1.3f}\t{}\t{}\t{}\t{:1.3f}".format(t, tp, fp, fn, p))
        prec.append(p)
    #print("AP\t-\t-\t-\t{:1.3f}".format(np.mean(prec)))
    
    return np.mean(prec)

# Precision helper function
def precision_at(threshold, iou):
    matches = iou > threshold
    true_positives = np.sum(matches, axis=1) == 1   # Correct objects
    false_positives = np.sum(matches, axis=0) == 0  # Missed objects
    false_negatives = np.sum(matches, axis=1) == 0  # Extra objects
    tp, fp, fn = np.sum(true_positives), np.sum(false_positives), np.sum(false_negatives)
    return tp, fp, fn

# ============================================== Other metrics ====================================

def accuracy(gt, pr):
    return int(gt == pr)

def iou_score(gt, pr):
    intersection = (gt * pr).sum()
    union = (gt + pr).sum() - intersection
    return (intersection + 10e-5)/(union + 10e-5)