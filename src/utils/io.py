import cv2
import numpy as np
from skimage.morphology import label
from skimage.morphology import disk


def read_image(path, **kwargs):
    return cv2.imread(path, **kwargs)[..., ::-1]

def save_image(path, image, **kwargs):
    cv2.imwrite(path, image, **kwargs)

# ref: https://www.kaggle.com/paulorzp/run-length-encode-and-decode
def rle_encode(img):
    """
    :param img: numpy array, 1 - mask, 0 - background
    :return: run length as string formated
    """
    pixels = img.T.flatten()
    pixels = np.concatenate([[0], pixels, [0]])
    runs = np.where(pixels[1:] != pixels[:-1])[0] + 1
    runs[1::2] -= runs[::2]
    return ' '.join(str(x) for x in runs)


def multi_rle_encode(img):
    labels = label(img[:, :, 0], connectivity=1)
    return [rle_encode(labels == k) for k in np.unique(labels[labels > 0])]


def rle_decode(rle_mask, shape):
    """
    :param rle_mask: run-length as string formated (start length)
    :param shape: (height,width) of array to return
    :return: numpy array, 1 - mask, 0 - background
    """
    s = rle_mask.split()
    starts, lengths = [np.asarray(x, dtype=int) for x in (s[0:][::2], s[1:][::2])]
    starts -= 1
    ends = starts + lengths
    img = np.zeros(shape[0]*shape[1], dtype=np.uint8)
    for lo, hi in zip(starts, ends):
        img[lo:hi] = 1
    return img.reshape(shape).T  # Needed to align to RLE direction


def multi_rle_decode(rle_masks: list, shape: tuple):
    # decode multiple RLE masks to one image
    all_masks = np.zeros(shape, dtype=np.int16)
    for mask in rle_masks:
        if isinstance(mask, str):
            all_masks += rle_decode(mask, shape=shape)
    return np.expand_dims(all_masks, -1)


def masks_as_overlap(rle_masks, shape, thickness=1):
    # decode multiple RLE masks to one image of masks overlapping regions
    # in other words return overlaps of input rle masks
    all_masks = np.zeros(shape, dtype=np.int16)
    kernel = disk(thickness)

    for mask in rle_masks:
        if isinstance(mask, str):
            bin_mask = rle_decode(mask, shape=shape)
            bin_mask = cv2.dilate(bin_mask, kernel)
            all_masks += bin_mask

    overlap_masks = (all_masks > 1).astype(np.uint8)
    return np.expand_dims(overlap_masks, -1)