import os
import git
import toml
import shutil

def get_short_commit_hash(lenght=7):
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    short_sha = repo.git.rev_parse(sha, short=lenght)
    return short_sha

def save_commit_hash(directory):
    path = os.path.join(directory, 'commit.txt')
    commit_hash = get_short_commit_hash()
    with open(path, 'w') as f:
        f.write(commit_hash)
        
def save_config(config, directory):
    dst_path = os.path.join(directory, 'config.toml')
    
    if isinstance(config, dict):
        with open(dst_path, 'w') as f:
            toml.dump(config, f)
    elif isinstance(config, str):
        shutil.copyfile(config, dst_path)
    else:
        raise ValueError('config should be dict or path to config file')
    